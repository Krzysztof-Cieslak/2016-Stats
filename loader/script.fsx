#r "../packages/Octokit/lib/net45/Octokit.dll"
#load "octokit.fsx"
#r "../packages/Newtonsoft.Json/lib/net45/Newtonsoft.Json.dll"

open System
open System.IO
open Newtonsoft.Json
open Octokit

//Load Username and Password / Key from file
let auth = File.ReadAllLines("auth.data")
let username, password = auth.[0], auth.[1]

// Create client
let client =
    Octokit.createClient username password
    |> Async.RunSynchronously

// Get User
let user = client.User.Get(username) |> Async.AwaitTask |> Async.RunSynchronously

//Search for all PRS in 2016 (handles paging)
let prs =
    async {
        let getIssues i =
            let r = SearchIssuesRequest()
            r.Type <- Nullable (unbox 0)
            r.Author <- username
            r.Created <- DateRange(DateTime(2016,01,01), SearchQualifierOperator.GreaterThanOrEqualTo)
            r.Page <- i
            r |> client.Search.SearchIssues |> Async.AwaitTask
        let! resp = getIssues 1
        if resp.TotalCount > 100 then
            let pages = float resp.TotalCount / 100. |> Math.Ceiling |> int
            let! pagesResp = [2 .. pages] |> List.map (fun i -> getIssues i) |> Async.Parallel
            return
                pagesResp
                |> Seq.collect (fun i -> i.Items)
                |> Seq.append resp.Items
                |> Seq.toList
        else
            return
                resp.Items
                |> Seq.toList
    } |> Async.RunSynchronously

//Get repositories based on PR list
let reposFromPRs =
    prs
    |> List.map (fun n ->
        let t = n.Url.ToString().Split([|'/'|], StringSplitOptions.RemoveEmptyEntries )
        t.[3], t.[4])
    |> List.distinct
    |> List.map (client.Repository.Get >> Async.AwaitTask >> Async.RunSynchronously  )

//Get all user repositories (without paging)
let userRepos =
    client.Repository.GetAllForUser(username)
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> Seq.toList

//Get repositories from Orgs (<> MSFT)
let orgRepos =
    client.Organization.GetAllForCurrent()
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> Seq.toList
    |> List.where(fun o -> o.Login <> "Microsoft")
    |> List.collect (fun o ->
        client.Repository.GetAllForOrg o.Login
        |> Async.AwaitTask
        |> Async.RunSynchronously
        |> Seq.toList
    )

//Get all distinct repos, remove forks, remove private (only OSS)
let allRepos =
    [
        yield! reposFromPRs
        yield! userRepos
        yield! orgRepos
    ] |> List.distinctBy (fun r -> r.Id) |> List.where (fun r -> not r.Fork && not r.Private)

// Save repos to JSON
Newtonsoft.Json.JsonConvert.SerializeObject(allRepos) |> fun c -> System.IO.File.WriteAllText("repos.json",c)


let commits =
    let getCommits (repo : Repository) =
        async {
            try
                let! res = client.Repository.Commit.GetAll(repo.Id) |> Async.AwaitTask
                return res |> Seq.toList |> List.map (fun c -> repo,c)
            with
            | _ -> return []
        }

    allRepos
    |> List.chunkBySize 30
    |> List.mapi (fun i chunk ->
        let r=
            chunk
            |> List.map (getCommits)
            |> Async.Parallel
            |> Async.RunSynchronously
            |> Seq.collect id
            |> Seq.toList
        printfn "Chunk %d done" i
        let fn = sprintf "data%d.json" i
        Newtonsoft.Json.JsonConvert.SerializeObject(allRepos) |> fun c -> System.IO.File.WriteAllText(fn,c)
        System.Threading.Thread.Sleep(TimeSpan.FromMinutes 1.)
        r
    )
    |> List.collect id


let lastYearCommits =
    commits
    |> List.where (fun (_,x) -> x.Commit.Author.Date.DateTime > DateTime(2015,12,31) && x.Commit.Author.Date.DateTime < DateTime(2017,01,01))
    |> List.where (fun (_,x) -> x.Commit.Author.Email = user.Email)



let groupedByRepo =
    lastYearCommits
    |> List.map (fun (res,x) -> res.FullName, x.Commit.Author.Date)
    |> List.groupBy (fun (res,x) -> res)
    |> List.map (fun (res, xs) -> res,xs |> List.map snd)
    |> List.sortByDescending (fun (_, xs) -> xs.Length)

// Save commits to JSON
Newtonsoft.Json.JsonConvert.SerializeObject(groupedByRepo) |> fun c -> System.IO.File.WriteAllText("data/commits.json",c)


let groupedByMonth =
    groupedByRepo
    |> List.collect (fun (res, xs) -> xs |> List.map (fun x -> res,x) )
    |> List.groupBy (fun (res,x) -> x.Month )
    |> List.sortByDescending (fun (_, xs) -> xs.Length)

let groupedByDayOfWeek =
    groupedByRepo
    |> List.collect (fun (res, xs) -> xs |> List.map (fun x -> res,x) )
    |> List.groupBy (fun (res,x) -> x.DayOfWeek )
    |> List.sortByDescending (fun (_, xs) -> xs.Length)

type TimeOfDay =
| Night
| Morning
| Afternoon
| Evening

let toTimeOfDay (timespan : TimeSpan) =
    if timespan.Hours < 8 then Night
    elif timespan.Hours < 12 then Morning
    elif timespan.Hours < 17 then Afternoon
    elif timespan.Hours < 22 then Evening
    else Night

let groupedByDayOfWeek =
    groupedByRepo
    |> List.collect (fun (res, xs) -> xs |> List.map (fun x -> res,x) )
    |> List.groupBy (fun (res,x) -> x.TimeOfDay |> toTimeOfDay )
    |> List.sortByDescending (fun (_, xs) -> xs.Length)


